﻿using PubMarialvaAspNet.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PubMarialvaAspNet.Controllers
{
    public class HomeController : Controller
    {
        PubMarialvaAspNetContext db = new PubMarialvaAspNetContext();

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Local()
        {
            return View();
        }

        public ActionResult Menu()
        {
            IEnumerable<Entrada> entradas = db.Entradas.ToList();

            IEnumerable<Prato> pratos = db.Pratos.ToList();

            IEnumerable<Acompanhamento> acompanhamentos = db.Acompanhamentos.ToList();

            ViewModelMenu viewModelMenu = new ViewModelMenu(entradas, pratos, acompanhamentos);

            return View(viewModelMenu);
        }
    }
}