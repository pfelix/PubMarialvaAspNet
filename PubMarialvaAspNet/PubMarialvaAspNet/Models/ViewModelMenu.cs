﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PubMarialvaAspNet.Models
{
    public class ViewModelMenu
    {

        public IEnumerable<Entrada> Entradas { get; private set; }
        public IEnumerable<Prato> Pratos { get; private set; }
        public IEnumerable<Acompanhamento> Acompanhamentos { get; private set; }

        /// <summary>
        /// Modelos para conseguir enviar para a view os dados das tabelas Entrada, Prato e Acompanhamento
        /// </summary>
        /// <param name="entradas"></param>
        /// <param name="pratos"></param>
        /// <param name="acompanhamentos"></param>
        public ViewModelMenu(IEnumerable<Entrada> entradas, IEnumerable<Prato> pratos, IEnumerable<Acompanhamento> acompanhamentos)
        {
            Entradas = entradas;
            Pratos = pratos;
            Acompanhamentos = acompanhamentos;
        }
    }
}