﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace PubMarialvaAspNet.Models
{
    public class Acompanhamento
    {
        [Key]
        public string AcompanhamentoId { get; set; }

        public string Acompanhamentos { get; set; }

    }
}