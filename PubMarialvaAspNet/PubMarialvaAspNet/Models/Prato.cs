﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace PubMarialvaAspNet.Models
{
    public class Prato
    {
        [Key]
        public string PratoId { get; set; }

        public string Pratos { get; set; }

    }
}