﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace PubMarialvaAspNet.Models
{
    public class Entrada
    {
        [Key]
        public string EntradasId { get; set; }

        public string EntradaPrato { get; set; }
    }
}