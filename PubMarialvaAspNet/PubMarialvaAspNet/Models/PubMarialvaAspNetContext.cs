﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace PubMarialvaAspNet.Models
{
    public class PubMarialvaAspNetContext : DbContext
    {
        // You can add custom code to this file. Changes will not be overwritten.
        // 
        // If you want Entity Framework to drop and regenerate your database
        // automatically whenever you change your model schema, please use data migrations.
        // For more information refer to the documentation:
        // http://msdn.microsoft.com/en-us/data/jj591621.aspx
    
        public PubMarialvaAspNetContext() : base("name=PubMarialvaAspNetContext")
        {
        }

        public System.Data.Entity.DbSet<PubMarialvaAspNet.Models.Reserva> Reservas { get; set; }

        public System.Data.Entity.DbSet<PubMarialvaAspNet.Models.Entrada> Entradas { get; set; }

        public System.Data.Entity.DbSet<PubMarialvaAspNet.Models.Prato> Pratos { get; set; }

        public System.Data.Entity.DbSet<PubMarialvaAspNet.Models.Acompanhamento> Acompanhamentos { get; set; }
    }
}
