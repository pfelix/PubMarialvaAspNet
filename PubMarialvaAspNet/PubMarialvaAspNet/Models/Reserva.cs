﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace PubMarialvaAspNet.Models
{
    public class Reserva
    {
        [Key]
        public int ReservaId { get; set; }

        [Required]
        public string Nome { get; set; }

        [Required]
        [Display(Name = "Lugares")]
        public int NumeroPessoas { get; set; }

        [Required]
        [DataType(DataType.DateTime)]
        [Display(Name = "Data")]
        public DateTime DataReserva { get; set; }
    }
}